from django.forms import ModelForm
from .models import Post, Comment


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'title',
            'date',
            'text',
        ]
        labels = {
            'title': 'Title',
            'date': 'Date',
            'text': 'Text',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'date',
            'text',
        ]
        labels = {
            'author': 'User',
            'date': 'Date',
            'text': 'Comment',
        }