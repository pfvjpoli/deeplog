from .models import Post, Comment, Category
from .forms import CommentForm
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect

class PostListView(generic.ListView):
    model = Post
    template_name = 'posts/list.html'

class PostDetailView(generic.DetailView):
    model = Post
    template_name = 'posts/detail.html'

class PostCreateView(generic.CreateView):
    model = Post
    fields = ['title', 'date', 'text']
    template_name = 'posts/create.html'
    def get_success_url(self):
        return reverse('detail', args=(self.object.pk,))

class PostUpdateView(generic.UpdateView):
    model = Post
    fields = ['title', 'date', 'text']
    template_name = 'posts/update.html'
    def get_success_url(self):
        return reverse('detail', args=(self.object.pk,))

class PostDeleteView(generic.DeleteView):
    model = Post
    template_name = 'posts/delete.html'
    def get_success_url(self):
        return reverse('list')

def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_date =  form.cleaned_data['date']
            comment_text = form.cleaned_data['text']
            
            comment = Comment(author=comment_author,
                            date=comment_date,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'posts/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'posts/categories.html'

class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'posts/category_detail.html'