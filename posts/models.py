from django.db import models
from django.conf import settings

class Post(models.Model):
    title = models.CharField(max_length=255)
    date = models.DateTimeField()
    text = models.TextField(max_length=1000)

    class Meta:
        ordering = ['-date'] #Django magic

    def __str__(self):
        return f'{self.title} ({self.date})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    date = models.DateTimeField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-date'] #Django magic

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    name = models.CharField(max_length=255)
    posts = models.ManyToManyField(Post)
    text = models.TextField(max_length=500, default='A cool category!')

    def __str__(self):
        return f'{self.name}'


