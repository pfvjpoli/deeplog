# Generated by Django 3.2.9 on 2021-12-04 17:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0003_rename_list_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='text',
            field=models.TextField(default='A cool category!', max_length=500),
        ),
    ]
